# Cool Links.

## Addendum, early...

NOTE! original Github project, is located at:

  https://github.com/upvalue/upvalue-io-code/tree/master/cool-links

A simple link shortener, written with Flask and Intercooler. The goal
was just to see how far I could get without writing any JS.

Writeup is here: [JavaScript without JavaScript](https://upvalue.io/javascript-without-javascript-intercooler-js/)

## Build & Run

```shell

  npm install package.json

  pip install poetry
  poetry init              # NOTE the virtual-env created!
  poetry add flask-peewee

  . $HOME/.cache/pypoetry/virtualenvs/cool-links-py3.7/bin/activate

  python app.py

```

